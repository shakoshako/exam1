package com.example.finalexamex

data class Item(
    var id: Int,
    var imageUrl: String,
    val title: String,
    val description: String,
)
