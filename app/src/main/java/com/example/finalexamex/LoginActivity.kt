package com.example.finalexamex

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var textView: TextView
    private lateinit var buttonChangePassword: Button
    //private lateinit var buttonLogout: Button
    private lateinit var buttonAddStatements: Button
    private lateinit var buttonStatements: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()

        registerListeners()

        textView.text = "გამარჯობა, " + FirebaseAuth.getInstance().currentUser?.email + "!"

        buttonLogout.setOnClickListener {

            AlertDialog.Builder(this)
                .setTitle("გამოსვლა")
                .setMessage("ნამდვილად გსურთ გამოსვლა?")
                .setPositiveButton("დიახ") { dialog, _ ->
                    FirebaseAuth.getInstance().signOut()
                    startActivity(Intent(this, MainActivity::class.java))
                    Toast.makeText(this, "გამოსვლა განხორციელდა.", Toast.LENGTH_SHORT).show()
                    finish()
                    dialog.dismiss()
                }
                .setNegativeButton("არა") { dialog, _ ->
                    dialog.dismiss()
                }
                .setNeutralButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
        }
    }

    private fun init() {
        textView = findViewById(R.id.textView)
        buttonChangePassword = findViewById(R.id.buttonChangePassword)
        //buttonLogout = findViewById(R.id.buttonLogout)
        buttonAddStatements = findViewById(R.id.buttonAddStatements)
        buttonStatements = findViewById(R.id.buttonStatements)
    }

    private fun registerListeners() {
        buttonChangePassword.setOnClickListener {
            startActivity(Intent(this, ChangePasswordActivity::class.java))
        }
        buttonAddStatements.setOnClickListener {
            startActivity(Intent(this, CreateStatementsActivity::class.java))
        }
        buttonStatements.setOnClickListener {
            startActivity(Intent(this, StatementsActivity::class.java))
        }
        //buttonLogout.setOnClickListener {
            //FirebaseAuth.getInstance().signOut()
            //startActivity(Intent(this, MainActivity::class.java))
            //finish()
        //}
    }

}