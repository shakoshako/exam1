package com.example.finalexamex

data class UserInfo(
    val title: String = "",
    val description: String = "",
    val url: String = "",
)
