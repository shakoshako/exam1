package com.example.finalexamex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView

class StatementsActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guest)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RecyclerViewItemAdapter(getItemData())

    }

    private fun getItemData(): List<Item> {
        val itemList = ArrayList<Item>()


        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )
        itemList.add(
            Item(
                1,
                "https://www.maserati.com/content/dam/maserati/international/Models/mc20/introduction/01-desktop.jpg",
                "For Sale",
                "adsads das asd asd as",
            )
        )


        return itemList
    }
}