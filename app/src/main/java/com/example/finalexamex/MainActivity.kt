package com.example.finalexamex

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonLogin: Button
    private lateinit var buttonRegister: Button
    private lateinit var buttonResetPassword: Button
    private lateinit var buttonGuest: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        init()

        registerListeners()

    }


    private fun init(){

        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonLogin = findViewById(R.id.buttonLogin)
        buttonRegister = findViewById(R.id.buttonRegister)
        buttonResetPassword = findViewById(R.id.buttonResetPassword)
        buttonGuest = findViewById(R.id.buttonGuest)
    }

    private fun registerListeners(){
        buttonRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        buttonResetPassword.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }

        buttonGuest.setOnClickListener {
            startActivity(Intent(this, StatementsActivity::class.java))
        }

        buttonLogin.setOnClickListener {

            val email = editTextEmail.text.toString()
            val password = editTextEmail.text.toString()

            if (email.isEmpty() || password.isEmpty()){
                Toast.makeText(this, "ცარიელია.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener{ task ->
                    if (task.isSuccessful) {
                        goToProfile()
                    } else {
                        Toast.makeText(this, "თქვენ არ ხართ რეგისტრირებული!", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

    private fun goToProfile() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }


}